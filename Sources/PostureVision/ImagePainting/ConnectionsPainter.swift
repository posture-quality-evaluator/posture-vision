//
//  ConnectionsPainter.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import UIKit
import SwiftyBeaver


public final class PVConnectionsPainter: PVConnectionsPainterProtocol {

    // MARK: - API

    public let configuration: PVConnectionsPainterConfiguration

    public init(configuration: PVConnectionsPainterConfiguration) {
        self.configuration = configuration
        self.workQueue = DispatchQueue(label: "posture.ios.connections-painter", qos: .userInitiated)
        self.logger = SwiftyBeaver.self
    }

    public func paint(connections: [PVLine], over image: UIImage, usingCompletionQueue queue: DispatchQueue, completion: @escaping Completion) {
        paint(connections: connections, over: image) { result in
            queue.async {
                completion(result)
            }
        }
    }

    // MARK: - Private

    private let workQueue: DispatchQueue
    private let logger: SwiftyBeaver.Type

    private func paint(connections: [PVLine], over image: UIImage, completion: @escaping Completion) {
        workQueue.async {
            let result = self.paint(connections: connections, over: image)
            completion(result)
        }
    }

    private func paint(connections: [PVLine], over originalImage: UIImage) -> UIImage {
        var resultImage = originalImage.grayed

        let renderer = UIGraphicsImageRenderer(size: resultImage.size)

        resultImage = renderer.image { context in
            resultImage.draw(at: .zero)

            draw(
                connections: connections,
                lineWidth: max(originalImage.size.width / 100, 1.0),
                drawPoints: true,
                alpha: 1.0,
                on: context.cgContext
            )
        }

        return resultImage
    }

    private func draw(connections: [PVLine], lineWidth: CGFloat, drawPoints: Bool, alpha: CGFloat, on context: CGContext) {
        let radius = Double(lineWidth)
        let offset: Double = 4

        for c in connections {
            let connectionColor = color(for: c)

            let startPoint = CGPoint(
                x: c.firstPoint.location.x + offset,
                y: c.firstPoint.location.y + offset
            )

            let endPoint = CGPoint(
                x: c.secondPoint.location.x + offset,
                y: c.secondPoint.location.y + offset
            )

            context.setStrokeColor(connectionColor.cgColor)
            context.setAlpha(alpha)
            context.setLineWidth(lineWidth)
            context.beginPath()

            context.move(to: startPoint)
            context.addLine(to: endPoint)

            if drawPoints {
                let points = [c.firstPoint, c.secondPoint]

                for point in points {
                    let x = point.location.x - radius + offset
                    let y = point.location.y - radius + offset
                    let color = self.color(for: point.type)

                    let ellipseRect = CGRect(
                        origin: CGPoint(x: x, y: y),
                        size: CGSize(width: 2 * radius, height: 2 * radius)
                    )

                    context.setStrokeColor(color.cgColor)
                    context.addEllipse(in: ellipseRect)
                }
            }

            context.strokePath()
        }
    }

    private func color(for connection: PVLine) -> UIColor {
        let key = PairKey<PVPointType>(connection.firstPoint.type, connection.secondPoint.type)
        if let color = configuration.connectionsColors[key] {
            return color
        }

        logger.warning("Couldn't find color in configuration for connection by key: \(key)")

        return UIColor.gray
    }

    private func color(for point: PVPointType) -> UIColor {
        if let color = configuration.pointsColors[point] {
            return color
        }

        logger.warning("Couldn't find color in configuration for point: \(point)")

        return UIColor.gray
    }
}
