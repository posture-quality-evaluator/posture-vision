//
//  ConnectionsPainterProtocol.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import UIKit


public protocol PVConnectionsPainterProtocol {
    typealias Completion = (UIImage) -> Void

    func paint(connections: [PVLine], over image: UIImage, usingCompletionQueue queue: DispatchQueue, completion: @escaping Completion)
}
