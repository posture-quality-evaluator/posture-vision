//
//  Errors.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import Foundation


public enum PVError: Error {
    case failedToCreateCIImage
    case failedToCreateVisionModel
    case selfInstanceDeinited
    case unknownModelOutputFormat
    case couldNotExtractOutputFeatures
    case invalidModelOutputShape
    case mlRequestPerformingError
}
