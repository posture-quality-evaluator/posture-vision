//
//  PairKey.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import Foundation


public struct PairKey<T: Hashable>: Hashable {
    public let first: T
    public let second: T

    public init(_ first: T, _ second: T) {
        self.first = first
        self.second = second
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(first)
        hasher.combine(second)
    }

    public static func ==(lhs: PairKey, rhs: PairKey) -> Bool {
        return lhs.first == rhs.first && lhs.second == rhs.second
    }
}
