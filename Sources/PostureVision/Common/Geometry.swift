//
//  Geometry.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import CoreGraphics


public enum PVPointType: Hashable {
    case head, neck, rShoulder, rElbow, rWrist, lShoulder, lElbow, lWrist, rHip, rKnee, rAnkle, lHip, lKnee, lAnkle, chest, background
}

public struct PVConnectionType {
    public var firstPointType: PVPointType
    public var secondPointType: PVPointType
    public var pafIndices: (Int, Int)

    public init(_ firstPointType: PVPointType, _ secondPointType: PVPointType, pafIndices: (Int, Int) = (-1, -1)) {
        self.firstPointType = firstPointType
        self.secondPointType = secondPointType
        self.pafIndices = pafIndices
    }
}

public struct PVLine {
    public var firstPoint: PVPoint
    public var secondPoint: PVPoint
    var score: Float32
    var firstOffset: Int
    var secondOffset: Int
}

public struct PVPoint {
    public var type: PVPointType
    public var location: PVPointLocation
}

extension PVPoint: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(location)
    }
}

public struct PVPointLocation {
    public var x: Double
    public var y: Double
    public var confidence: Double
}

extension PVPointLocation: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
        hasher.combine(confidence)
    }
}

public struct PVMeasurement {
    public var points: [PVPointType: PVPointLocation]

    public init(points: [PVPointType: PVPointLocation]) {
        self.points = points
    }
}
