//
//  Configurations.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import UIKit


public struct PVPointsDetectorConfiguration {
    public var layersCount: Int
    public var outputWidth: Int
    public var outputHeight: Int
    public var inputSize: CGSize
    public var backgroundLayerIndex: Int

    public init(layersCount: Int, outputWidth: Int, outputHeight: Int, inputSize: CGSize, backgroundLayerIndex: Int) {
        self.layersCount = layersCount
        self.outputWidth = outputWidth
        self.outputHeight = outputHeight
        self.inputSize = inputSize
        self.backgroundLayerIndex = backgroundLayerIndex
    }
}

public struct PVHeatmapToPointsTransformerConfiguration {
    public var layersCount: Int
    public var outputWidth: Int
    public var outputHeight: Int
    public var points: [PVPointType]
    public var minNmsThreshold: Float32
    public var maxNmsThreshold: Float32
    public var nmsWindowSize: Int

    public init(layersCount: Int, outputWidth: Int, outputHeight: Int, points: [PVPointType], minNmsThreshold: Float32, maxNmsThreshold: Float32, nmsWindowSize: Int) {
        self.layersCount = layersCount
        self.outputWidth = outputWidth
        self.outputHeight = outputHeight
        self.points = points
        self.minNmsThreshold = minNmsThreshold
        self.maxNmsThreshold = maxNmsThreshold
        self.nmsWindowSize = nmsWindowSize
    }
}

public struct PVPointsConnectorConfiguration {
    public var connectionTypes: [PVConnectionType]
    public var singlePerson: Bool
    public var pafLayerStartIndex: Int
    public var interMinAboveThreshold: Float32
    public var interThreshold: Float32
    public var outputWidth: Int
    public var outputHeight: Int

    public init(connectionTypes: [PVConnectionType], singlePerson: Bool, pafLayerStartIndex: Int, interMinAboveThreshold: Float32, interThreshold: Float32, outputSize: CGSize) {
        self.connectionTypes = connectionTypes
        self.singlePerson = singlePerson
        self.pafLayerStartIndex = pafLayerStartIndex
        self.interMinAboveThreshold = interMinAboveThreshold
        self.interThreshold = interThreshold
        self.outputWidth = Int(outputSize.width)
        self.outputHeight = Int(outputSize.height)
    }
}

public struct PVConnectionsPainterConfiguration {
    public var pointsColors: [PVPointType: UIColor]
    public var connectionsColors: [PairKey<PVPointType>: UIColor]

    public init(pointsColors: [PVPointType : UIColor], connectionsColors: [PairKey<PVPointType> : UIColor]) {
        self.pointsColors = pointsColors
        self.connectionsColors = connectionsColors
    }
}

public struct PVPointsTranslatorConfiguration {
    public var outputSize: CGSize

    public init(outputSize: CGSize) {
        self.outputSize = outputSize
    }
}
