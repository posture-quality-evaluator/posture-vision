//
//  File.swift
//  
//
//  Created by Александр Пахомов on 13.05.2020.
//

import Foundation


func deg2rad(_ number: Double) -> Double {
    return number * .pi / 180
}

func rad2deg(_ rad: Double) -> Double {
    return rad / .pi * 180
}
