//
//  PointsTranslatorProtocol.swift
//  
//
//  Created by Александр Пахомов on 12.05.2020.
//

import QuartzCore


public protocol PVPointsTranslatorProtocol: class {
    func translate(_ lines: [PVLine], usingOriginalImageSize imageSize: CGSize) -> [PVLine]
}
