//
//  PointsTranslator.swift
//  
//
//  Created by Александр Пахомов on 12.05.2020.
//

import QuartzCore


public final class PVPointsTranslator: PVPointsTranslatorProtocol {

    // MARK: - API

    public let configuration: PVPointsTranslatorConfiguration

    public init(configuration: PVPointsTranslatorConfiguration) {
        self.configuration = configuration
    }

    public func translate(_ lines: [PVLine], usingOriginalImageSize imageSize: CGSize) -> [PVLine] {
        return lines.map { line -> PVLine in
            let firstPoint = PVPoint(
                type: line.firstPoint.type,
                location: translate(line.firstPoint.location, intoPointLocationUsing: imageSize)
            )

            let secondPoint = PVPoint(
                type: line.secondPoint.type,
                location: translate(line.secondPoint.location, intoPointLocationUsing: imageSize)
            )

            return PVLine(firstPoint: firstPoint, secondPoint: secondPoint, score: line.score, firstOffset: line.firstOffset, secondOffset: line.secondOffset)
        }
    }

    // MARK: - Private

    private func translate(_ rawPointLocation: PVPointLocation, intoPointLocationUsing originalImageSize: CGSize) -> PVPointLocation {
        var scaleFactor = originalImageSize.height / configuration.outputSize.height

        var offsetX = CGFloat.zero
        var offsetY = CGFloat.zero

        if originalImageSize.width < originalImageSize.height {
            offsetX = 0.5 * (originalImageSize.width - configuration.outputSize.width * scaleFactor)
        } else {
            scaleFactor = originalImageSize.width / configuration.outputSize.width
            offsetY = 0.5 * (originalImageSize.height - configuration.outputSize.height * scaleFactor)
        }

        return PVPointLocation(
            x: rawPointLocation.x * Double(scaleFactor) + Double(offsetX),
            y: rawPointLocation.y * Double(scaleFactor) + Double(offsetY),
            confidence: rawPointLocation.confidence
        )
    }
}
