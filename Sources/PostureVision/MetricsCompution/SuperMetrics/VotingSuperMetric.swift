//
//  VotingSuperMetric.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import Foundation


public final class VotingSuperMetric: PVSuperMetricProtocol {
    public init() {

    }
    
    public func makeSuperDecision(on results: [PVMetricResultProtocol]) -> PVSuperMetricDecisionProtocol {
        let countPositive = results.filter { $0.decision }.count
        let superDecision = countPositive >= results.count
        return SimpleSuperMetricDecision(superDecision: superDecision)
    }
}
