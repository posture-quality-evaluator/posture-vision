//
//  SuperMetricProtocol.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import Foundation


public protocol PVSuperMetricProtocol: class {
    func makeSuperDecision(on results: [PVMetricResultProtocol]) -> PVSuperMetricDecisionProtocol
}

public protocol PVSuperMetricDecisionProtocol: class, CustomDebugStringConvertible {
    var superDecision: Bool { get }
}

public class SimpleSuperMetricDecision: PVSuperMetricDecisionProtocol {
    public let superDecision: Bool

    public var debugDescription: String {
        "Simple super metric decision: \(superDecision)"
    }

    public init(superDecision: Bool) {
        self.superDecision = superDecision
    }
}
