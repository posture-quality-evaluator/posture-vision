//
//  LeftShoulderMetric.swift
//  
//
//  Created by Александр Пахомов on 13.05.2020.
//

import Foundation


final public class PVLeftShoulderMetric: PVMetricProtocol {
    public init() {

    }

    public let requiredPointType: [PVPointType] = [
        .neck,
        .lShoulder
    ]

    public func compute(on measurement: PVMeasurement) -> PVMetricResultProtocol {
//        neck = measurement.joints[JointType.NECK]
//        right_shoulder = measurement.joints[JointType.RIGHT_SHOULDER]
//
//        x = neck.point.x - right_shoulder.point.x
//        y = neck.point.y - right_shoulder.point.y
//
//        if y != 0:
//            ang = math.degrees(math.atan(x / y))
//        else:
//            ang = 0
//
//        normal_coef = 5
//        normal = abs(ang) < normal_coef
//
//        coef = round(ang, 2)
//        return RightShoulderMetricResult(coef=coef)

        guard let neck = measurement.points[.neck], let lShoulder = measurement.points[.lShoulder] else {
            fatalError()
        }

        let x = neck.x - lShoulder.x
        let y = neck.y - lShoulder.y

        var ang: Double = 0
        if y != 0 {
            ang = rad2deg(atan(x / y))
        }

        let normalCoef: Double = 5
        let normal = abs(ang) < normalCoef

        let coef = round(abs(ang))
        return SimpleMetricResult(decision: normal, name: "Left Shoulder Angle")
    }
}
