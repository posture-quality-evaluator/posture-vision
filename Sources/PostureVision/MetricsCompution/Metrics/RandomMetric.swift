//
//  RandomMetric.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import Foundation


public final class RandomMetric: PVMetricProtocol {
    public let requiredPointType: [PVPointType] = []

    public init() {

    }

    public func compute(on measurement: PVMeasurement) -> PVMetricResultProtocol {
        let decision = Bool.random()
        return SimpleMetricResult(decision: decision, name: "Random")
    }
}
