//
//  MetricProtocol.swift
//  
//
//  Created by Александр Пахомов on 09.05.2020.
//

import Foundation


public protocol PVMetricProtocol: class {
    var requiredPointType: [PVPointType] { get }

    func compute(on measurement: PVMeasurement) -> PVMetricResultProtocol
}

public protocol PVMetricResultProtocol: class, CustomDebugStringConvertible {
    var decision: Bool { get }
}

public class SimpleMetricResult: PVMetricResultProtocol {
    public let decision: Bool
    public let name: String

    public var debugDescription: String {
        "\(name) decision: \(decision)"
    }

    init(decision: Bool, name: String) {
        self.decision = decision
        self.name = name
    }
}
