//
//  PointsConnectorProtocol.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import Foundation
import CoreML


public protocol PVPointsConnectorProtocol {
    func connect(mappedPoints: [PVPointType: [PVPointLocation]], heatmaps: MLMultiArray, originalImageSize: CGSize) -> [PVLine]
}
