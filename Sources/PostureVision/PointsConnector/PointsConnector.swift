//
//  PointsConnector.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import Foundation
import SwiftyBeaver
import CoreML


public final class PVPointsConnector: PVPointsConnectorProtocol {

    // MARK: - API

    public let configuration: PVPointsConnectorConfiguration

    public init(configuration: PVPointsConnectorConfiguration) {
        self.configuration = configuration
        self.logger = SwiftyBeaver.self
    }

    public func connect(mappedPoints: [PVPointType : [PVPointLocation]], heatmaps: MLMultiArray, originalImageSize: CGSize) -> [PVLine] {
        return construct(connectionsFrom: mappedPoints, and: heatmaps, usingTypes: configuration.connectionTypes, originalImageSize: originalImageSize)
    }

    // MARK: - Private

    private let logger: SwiftyBeaver.Type

    private func construct(connectionsFrom mappedPointLocations: [PVPointType: [PVPointLocation]], and heatmaps: MLMultiArray, usingTypes connectionTypes: [PVConnectionType], originalImageSize: CGSize) -> [PVLine] {
        var result: [PVLine] = []

        for connection in connectionTypes {
            let firstPVPointType = connection.firstPointType
            let secondPVPointType = connection.secondPointType
            let pafIndices = connection.pafIndices

            guard let firstPointLocations = mappedPointLocations[firstPVPointType], let secondPointLocations = mappedPointLocations[secondPVPointType] else {
                logger.info("Skipping connection type: \((firstPVPointType, secondPVPointType)). Required points not found")
                continue
            }

            if configuration.singlePerson {
                let newLines = construct(
                    linesFrom: firstPointLocations,
                    and: secondPointLocations,
                    assuming: firstPVPointType,
                    and: secondPVPointType
                )
                result.append(contentsOf: newLines)
            }
            else {
                let newLines = constructMultiple(
                    linesFrom: firstPointLocations,
                    and: secondPointLocations,
                    assuming: firstPVPointType,
                    and: secondPVPointType,
                    pafIndices: pafIndices,
                    heatmaps: heatmaps
                )

                result.append(contentsOf: newLines)
            }
        }

        var humanJoints: [Set<PVPoint>] = []
        var humanConnections: [Int: [PVLine]] = [:]

        result.enumerated().forEach { connIdx, c in
            var added = false
            (0..<humanJoints.count).forEach { humanIdx in
                let conn = humanJoints[humanIdx]
                if (conn.contains(c.firstPoint) && !conn.contains(c.secondPoint)) ||
                    (conn.contains(c.secondPoint) && !conn.contains(c.firstPoint)) {
                    humanJoints[humanIdx].insert(c.firstPoint)
                    humanJoints[humanIdx].insert(c.secondPoint)
                    humanConnections[humanIdx]?.append(c)
                    added = true
                    return
                }
            }
            if !added {
                humanJoints.append([c.firstPoint, c.secondPoint])
                humanConnections[humanJoints.count - 1] = [c]
            }
        }

        return humanConnections[0] ?? []
    }

    private func construct(linesFrom firstPointLocations: [PVPointLocation], and secondPointLocations: [PVPointLocation], assuming firstPVPointType: PVPointType, and secondPVPointType: PVPointType) -> [PVLine] {
        var result: [PVLine] = []

        for (firstPointIdx, firstPointLocation) in firstPointLocations.enumerated() {
            for (secondPointIdx, secondPointLocation) in secondPointLocations.enumerated() {
                let firstPoint = PVPoint(type: firstPVPointType, location: firstPointLocation)
                let secondPoint = PVPoint(type: secondPVPointType, location: secondPointLocation)

                let line = PVLine(
                    firstPoint: firstPoint,
                    secondPoint: secondPoint,
                    score: 0.0,
                    firstOffset: firstPointIdx,
                    secondOffset: secondPointIdx
                )

                result.append(line)
            }
        }

        return result
    }

    private func constructMultiple(linesFrom firstPointLocations: [PVPointLocation], and secondPointLocations: [PVPointLocation], assuming firstPVPointType: PVPointType, and secondPVPointType: PVPointType, pafIndices: (Int, Int), heatmaps: MLMultiArray) -> [PVLine] {

        let (indexX, indexY) = pafIndices
        let pafLayerStartIndex = configuration.pafLayerStartIndex
        let nnOutput = UnsafeMutablePointer<Float32>(OpaquePointer(heatmaps.dataPointer))
        let layerStride = heatmaps.strides[0].intValue

        let pafMatX = nnOutput.array(index: pafLayerStartIndex + indexX,
                                     count: layerStride)
        let pafMatY = nnOutput.array(index: pafLayerStartIndex + indexY,
                                     count: layerStride)

        var connectionCandidates: [PVLine] = []
        var connections: [PVLine] = []

        // Enumerate through non filtered joint connections
        firstPointLocations.enumerated().forEach { offset1, first in
            secondPointLocations.enumerated().forEach { offset2, second in

                let (x1, y1) = (first.x, first.y)
                let (x2, y2) = (second.x, second.y)

                let s = self.score(x1: Int(x1), y1: Int(y1), x2: Int(x2), y2: Int(y2),
                                        pafMatX: pafMatX, pafMatY: pafMatY,
                                        yStride: configuration.outputWidth)
                if s > 0 {
                    let firstPoint = PVPoint(type: firstPVPointType, location: first)
                    let secondPoint = PVPoint(type: secondPVPointType, location: second)

                    let line = PVLine(
                        firstPoint: firstPoint,
                        secondPoint: secondPoint,
                        score: s,
                        firstOffset: offset1,
                        secondOffset: offset2
                    )

                    connectionCandidates.append(line)
                }
            }
        }

        var (usedIdx1, usedIdx2) = (Set<Int>(), Set<Int>())
        connectionCandidates.sorted(by: { $0.score > $1.score }).forEach { c in
            if usedIdx1.contains(c.firstOffset) || usedIdx2.contains(c.secondOffset) {
                return
            }
            connections.append(c)
            usedIdx1.insert(c.firstOffset)
            usedIdx2.insert(c.secondOffset)
        }

        return connections
    }

    private func score(x1: Int, y1: Int, x2: Int, y2: Int,
               pafMatX: Array<Float32>, pafMatY: Array<Float32>,
               yStride: Int) -> Float32 {
        let vectorAToB = (x: Float32(x2 - x1), y: Float32(y2 - y1))
        let vectorAToBLength = (pow(vectorAToB.x, 2) + pow(vectorAToB.y, 2)).squareRoot()

        guard vectorAToBLength > 1e-6 else {
            return -1
        }

        let vectorAToBNorm = (x: vectorAToB.x / vectorAToBLength, y: vectorAToB.y / vectorAToBLength)
        var PAFs: [Float32] = []

        stride(x1: x1, y1: y1, x2: x2, y2: y2) { x, y, idx, count in
            // These offset values are used to cover larger area in PAF's to collect as many score as possible
            var dx = [-1, 0, 1]
            var dy = [0, 0, 0]
            if abs(x2 - x1) > abs(y2 - y1) {
                // If scores are collected horizontally then use Ys offsets instead of Xs
                //  When moving horizontally
                //  y - 1   y - 1      y - 1
                //  x1, y   x2, y .... xn, y
                //  y + 1   y + 1      y + 1
                //  When moving vertically
                //  x - 1   x, y1  x + 1
                //  x - 1   x, y2  x + 1
                //          ....
                //          ....
                //  x - 1   x, yn  x + 1
                swap(&dx, &dy)
            }

            for (dx, dy) in zip(dx, dy) {
                var offset = (y + dy) * yStride + x + dx
                // Check if it is out of array's bounds
                offset = min(max(0, offset), pafMatX.count - 1)
                // Accumulate PAFs values
                PAFs.append(vectorAToBNorm.x * pafMatX[offset] + vectorAToBNorm.y * pafMatY[offset])
            }
        }

        let pointCount = PAFs.count / 3 // it needs to be devided because there are three (dx, dy) pairs for each point
        PAFs = PAFs.filter { $0 > configuration.interThreshold }
        let sum = PAFs.reduce(0, +)
        let scoresCount = PAFs.count

        if Float32(scoresCount) / Float32(pointCount) > configuration.interMinAboveThreshold {
            return sum / Float32(scoresCount)
        }

        let threshold = Float32(configuration.outputWidth * configuration.outputHeight).squareRoot() / 150
        if vectorAToBLength < threshold {
            return 0.15
        }

        return -1
    }

    private func stride(x1: Int, y1: Int, x2: Int, y2: Int, processBlock: ((Int, Int, Int, Int) -> Void)) {
        let (dx, dy, stepCount) = delta(x1: x1, y1: y1, x2: x2, y2: y2)
        let (x0, y0) = (Float(x1), Float(y1))
        (0..<stepCount).forEach { idx in
            let x = Int((x0 + Float(idx) * dx).rounded(.toNearestOrAwayFromZero))
            let y = Int((y0 + Float(idx) * dy).rounded(.toNearestOrAwayFromZero))
            processBlock(x, y, idx, stepCount)
        }
    }

    private func delta(x1: Int, y1: Int, x2: Int, y2: Int) -> (dx: Float, dy: Float, stepCount: Int) {
        var (dx, dy) = (Float(abs(x2 - x1) + 1), Float(abs(y2 - y1) + 1))
        var stepCount  = (5 * max(abs(dx), abs(dy))).squareRoot()
        stepCount = max(5, min(25, stepCount))
        dx /= stepCount
        dy /= stepCount
        return (dx: dx, dy: dy, stepCount: Int(stepCount.rounded()))
    }
}
