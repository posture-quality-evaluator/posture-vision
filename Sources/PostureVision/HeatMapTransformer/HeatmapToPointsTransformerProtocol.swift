//
//  HeatmapToPointsTransformerProtocol.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import CoreML


public protocol PVHeatmapToPointsTransformerProtocol {
    typealias Completion = (Result<PVHeatmapToPointsTransformationResult, PVError>) -> Void

    func transform(heatmaps: MLMultiArray, originalImageSize: CGSize, usingCompletionQueue queue: DispatchQueue, completion: @escaping Completion)
}

public struct PVHeatmapToPointsTransformationStatistics {
    public var transformationDuration: TimeInterval
}

public struct PVHeatmapToPointsTransformationResult {
    public var statistics: PVHeatmapToPointsTransformationStatistics
    public var mappedPoints: [PVPointType: [PVPointLocation]]
}
