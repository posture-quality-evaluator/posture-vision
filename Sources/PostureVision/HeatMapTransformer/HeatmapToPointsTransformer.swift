//
//  HeatmapToPointsTransformer.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import CoreML
import SwiftyBeaver


public final class PVHeatmapToPointsTransformer: PVHeatmapToPointsTransformerProtocol {

    // MARK: - API

    public let configuration: PVHeatmapToPointsTransformerConfiguration

    public init(configuration: PVHeatmapToPointsTransformerConfiguration) {
        self.configuration = configuration
        self.workQueue = DispatchQueue(label: "posture.ios.heatmap_transformer", qos: .userInitiated)
        self.logger = SwiftyBeaver.self
    }

    /// Wraps another `transform` method and calls the completion on passed queue
    public func transform(heatmaps: MLMultiArray, originalImageSize: CGSize, usingCompletionQueue queue: DispatchQueue, completion: @escaping Completion) {
        transform(heatmaps: heatmaps, originalImageSize: originalImageSize) { result in
            queue.async {
                completion(result)
            }
        }
    }

    // MARK: - Private

    private let logger: SwiftyBeaver.Type
    private let workQueue: DispatchQueue

    private func transform(heatmaps: MLMultiArray, originalImageSize: CGSize, completion: @escaping Completion) {
        workQueue.async {
            let transformationStartTs = Date()

            let mappedPoints: [PVPointType: [HeatMapPointCandidate]] = self.transform(heatmaps: heatmaps)

            let translatedPoints: [PVPointType: [PVPointLocation]] = mappedPoints.mapValues { candidates -> [PVPointLocation] in
                return candidates.map {
                    self.translate($0, intoPointLocationUsing: originalImageSize)
                }
            }

            let transformationEndTs = Date()

            let statistics = PVHeatmapToPointsTransformationStatistics(
                transformationDuration: transformationEndTs.timeIntervalSince(transformationStartTs)
            )

            let result = PVHeatmapToPointsTransformationResult(
                statistics: statistics,
                mappedPoints: translatedPoints
            )

            completion(.success(result))
        }
    }

    private struct HeatMapPointCandidate {
        let col: Int
        let row: Int
        let layerIndex: Int
        let confidence: Float32
    }

    private func transform(heatmaps: MLMultiArray) -> [PVPointType: [HeatMapPointCandidate]] {
        let layerStride = heatmaps.strides[0].intValue

        return withExtendedLifetime(heatmaps) {
            let nnOutput = UnsafeMutablePointer<Float32>(OpaquePointer(heatmaps.dataPointer))
            let heatMatCount = configuration.points.count
            let heatMatPtr = nnOutput

            // Filter the heatmapp network output by applying a threshold
            let heatMapArray = heatMatPtr.array(index: 0, count: heatMatCount * layerStride).map { max(min($0, 1.0), 0.0) }

            // We expect that the output value from NN should lie between 0 and 1
            let heatMapCandidates = thresholdFilter(heatMapArray, heatMatPtr: heatMatPtr, layerStride: layerStride)

            // Continue filtering using a non maximum suppression approach
            let filteredHeatMapCandidates = maxSuppressionFilter(heatMapCandidates, pointsCount: configuration.points.count)

            // Map heat map layer index to joint type
            let candidatesByPoints = Dictionary(
                grouping: filteredHeatMapCandidates,
                by: { configuration.points[$0.layerIndex] }
            )

            return candidatesByPoints
        }
    }

    private func thresholdFilter(_ heatMapArray: [Float32], heatMatPtr: UnsafePointer<Float32>, layerStride: Int) -> [HeatMapPointCandidate] {
        let pointsCount = configuration.points.count
        let modelOutputWidth = configuration.outputWidth

        let avg = heatMapArray.reduce(0, +) / Float32(heatMapArray.count)
        var nmsThreshold: Float32 = configuration.minNmsThreshold
        nmsThreshold = max(avg * 4.0, nmsThreshold)
        nmsThreshold = min(nmsThreshold, configuration.maxNmsThreshold)

        var heatMapCandidates: [HeatMapPointCandidate] = []

        for layerIndex in 0..<pointsCount {
            let layerPtr = heatMatPtr.advanced(by: layerIndex * layerStride)
            for idx in 0..<layerStride {
                if layerPtr[idx] > nmsThreshold {
                    let col = idx % modelOutputWidth
                    let row = idx / modelOutputWidth
                    let candidate = HeatMapPointCandidate(col: col, row: row, layerIndex: layerIndex, confidence: layerPtr[idx])
                    heatMapCandidates.append(candidate)
                }
            }
        }

        return heatMapCandidates
    }

    private func maxSuppressionFilter(_ heatMapCandidates: [HeatMapPointCandidate], pointsCount: Int) -> [HeatMapPointCandidate] {
        var result: [HeatMapPointCandidate] = []

        let grouppedCandidates = Dictionary(grouping: heatMapCandidates, by: { $0.layerIndex })

        for (_, candidates) in grouppedCandidates {
            // Non maximum suppression to get as minimum candidates as possible
            let boxes = candidates.map { c -> BoundingBox in
                let originOffset = configuration.nmsWindowSize / 2

                let windowOrigin = CGPoint(
                    x: max(0, c.col - originOffset),
                    y: max(0, c.row - originOffset)
                )

                let windowSize = CGSize(
                    width: configuration.nmsWindowSize,
                    height: configuration.nmsWindowSize
                )

                return BoundingBox(
                    classIndex: 0,
                    score: Float(c.confidence),
                    rect: CGRect(origin: windowOrigin, size: windowSize)
                )
            }

            let boxIndices = nonMaxSuppression(boundingBoxes: boxes, iouThreshold: 0.01, maxBoxes: boxes.count)
            result += boxIndices.map { candidates[$0] }
        }

        return result
    }

    private func translate(_ heatmapCandidate: HeatMapPointCandidate, intoPointLocationUsing originalImageSize: CGSize) -> PVPointLocation {
        return PVPointLocation(
            x: Double(heatmapCandidate.col),
            y: Double(heatmapCandidate.row),
            confidence: Double(heatmapCandidate.confidence)
        )
    }
}

extension UnsafeMutablePointer where Pointee == Float32 {

    /// Returns an array with a specified index and size
    func array(index: Int, count: Int) -> Array<Pointee> {
        return Array(UnsafeBufferPointer(start: advanced(by: index * count), count: count))
    }
}
