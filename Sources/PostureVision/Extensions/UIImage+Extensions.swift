//
//  UIImage+Extensions.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import UIKit


extension UIImage {
    func resizedCentered(to newSize: CGSize) -> UIImage {
        let oldWidth = size.width
        let oldHeight = size.height
        let scaleFactor = size.height > size.width ? newSize.height / oldHeight : newSize.width / oldWidth

        let newSizeScaled = CGSize(width: oldWidth * scaleFactor, height: size.height * scaleFactor)

        let newOrigin = CGPoint(
            x: 0.5 * (newSize.width - newSizeScaled.width),
            y: 0.5 * (newSize.height - newSizeScaled.height)
        )

        let newRect = CGRect(
            origin: newOrigin,
            size: newSizeScaled
        )

        let format = UIGraphicsImageRendererFormat.default()
        format.scale = scale
        let renderer = UIGraphicsImageRenderer(size: newSize, format: format)
        let image = renderer.image { ctx in
            UIColor.white.setFill()
            ctx.fill(CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            draw(in: newRect)
        }

        return image
    }

    var grayed: UIImage {
        guard let grayingfilter = CIFilter(name: "CIPhotoEffectNoir") else {
            return self
        }

        let ciImage = self.ciImage ?? CIImage(image: self)
        grayingfilter.setValue(ciImage, forKey: kCIInputImageKey)

        guard let outputCIImage = grayingfilter.outputImage else {
            return self
        }
        return UIImage(ciImage: outputCIImage, scale: self.scale, orientation: self.imageOrientation)
    }
}
