//
//  PointsDetector.swift
//  
//
//  Created by Александр Пахомов on 07.05.2020.
//

import UIKit
import CoreML
import Vision
import SwiftyBeaver


public final class PVPointsDetector: PVPointsDetectorProtocol {

    // MARK: - API

    public let model: MLModel
    public let configuration: PVPointsDetectorConfiguration

    public init(model: MLModel, configuration: PVPointsDetectorConfiguration) {
        self.model = model
        self.configuration = configuration
        self.workQueue = DispatchQueue(label: "posture.ios.detector", qos: .userInitiated)
        self.logger = SwiftyBeaver.self
    }

    /// Wraps another `detectPoints` method and calls the completion on passed queue
    public func detectPoints(on image: UIImage, usingCompletionQueue queue: DispatchQueue, completion: @escaping Completion) throws {
        try detectPoints(on: image, completion: { result in
            queue.async {
                completion(result)
            }
        })
    }

    // MARK: - Private

    private let logger: SwiftyBeaver.Type
    private let workQueue: DispatchQueue

    /// Performs ML request and returns heatmaps in completion
    private func detectPoints(on originalImage: UIImage, completion: @escaping Completion) throws {
        logger.debug("Starting detection of points")

        var predictionStartTs: Date!
        var predictionEndTs: Date!

        // Resize, center the image and convert it to CIImage
        let image = try prepareImageForMLRequest(originalImage)

        // Create a ML request (the image will be passed in the handler below)
        let request = try mlRequest(model: self.model) { [weak self] result in
            predictionEndTs = Date()

            guard let self = self else {
                completion(.failure(.selfInstanceDeinited))
                return
            }

            switch result {
            case .success(let array):
                self.logger.info("Success of performing ML request.")

                let statistics = PVPointsDetectionStatistics(
                    predictionDuration: predictionEndTs.timeIntervalSince(predictionStartTs)
                )

                let result = PVPointsDetectionResult(
                    statistics: statistics,
                    heatmaps: array
                )

                completion(.success(result))

            case .failure(let error):
                self.logger.error("Error performing ML request: \(error).")

                completion(.failure(error))

            }
        }

        // Extra resizing and cropping property (although actually the image has already been prepared)
        request.imageCropAndScaleOption = .centerCrop

        let handler = VNImageRequestHandler(ciImage: image)
        workQueue.async {
            do {
                self.logger.debug("Starting ML request evaluation")

                predictionStartTs = Date()
                try handler.perform([request])

                self.logger.debug("Finishing ML request evaulation")
            } catch {
                self.logger.error("Error performing ML request with handler: \(error)")
                completion(.failure(.mlRequestPerformingError))
            }
        }
    }

    /// Resizes the image and converts it to CIImage
    private func prepareImageForMLRequest(_ image: UIImage) throws -> CIImage {
        var uiImage = image
        if image.size != configuration.inputSize {
            uiImage = image.resizedCentered(to: configuration.inputSize)
        }

        guard let ciImage = CIImage(image: uiImage) else {
            throw PVError.failedToCreateCIImage
        }

        return ciImage
    }

    /// Creates ML request and checks model output format before completion call
    private func mlRequest(model: MLModel, completion: @escaping (Result<MLMultiArray, PVError>) -> Void) throws -> VNCoreMLRequest {
        let vnModel: VNCoreMLModel!
        do {
            vnModel = try VNCoreMLModel(for: model)
        } catch {
            throw PVError.failedToCreateVisionModel
        }

        let coremlRequest = VNCoreMLRequest(model: vnModel) { [weak self] request, error in
            guard let self = self else {
                completion(.failure(PVError.selfInstanceDeinited))
                return
            }

            let config = self.configuration

            guard let observations = request.results as? [VNCoreMLFeatureValueObservation] else {
                completion(.failure(PVError.unknownModelOutputFormat))
                return
            }

            guard let outputMultirArray = observations.first?.featureValue.multiArrayValue else {
                completion(.failure(PVError.couldNotExtractOutputFeatures))
                return
            }

            let expectedOutputShape: [Int] = [
                config.layersCount,
                config.outputWidth,
                config.outputHeight
            ]

            guard outputMultirArray.shape == expectedOutputShape as [NSNumber] else {
                completion(.failure(PVError.invalidModelOutputShape))
                return
            }

            completion(.success(outputMultirArray))
        }

        return coremlRequest
    }
}
