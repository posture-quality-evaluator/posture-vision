//
//  PointsDetectorProtocol.swift
//
//
//  Created by Александр Пахомов on 07.05.2020.
//

import UIKit
import CoreML


public protocol PVPointsDetectorProtocol {
    typealias Completion = (Result<PVPointsDetectionResult, PVError>) -> Void

    func detectPoints(on image: UIImage, usingCompletionQueue queue: DispatchQueue, completion: @escaping Completion) throws
}

public struct PVPointsDetectionStatistics {
    public var predictionDuration: TimeInterval
}

public struct PVPointsDetectionResult {
    public var statistics: PVPointsDetectionStatistics
    public var heatmaps: MLMultiArray
}
