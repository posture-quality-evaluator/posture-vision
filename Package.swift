// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PostureVision",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "PostureVision",
            targets: ["PostureVision"]
        ),
    ],
    dependencies: [
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver.git", .upToNextMajor(from: "1.9.0")),
    ],
    targets: [
        .target(
            name: "PostureVision",
            dependencies: ["SwiftyBeaver"]
        ),
        .testTarget(
            name: "PostureVisionTests",
            dependencies: ["PostureVision"]
        ),
    ]
)
